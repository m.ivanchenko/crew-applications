import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { STAGES, STAGE_ORDER } from '../../constants';
import { jsonCopy } from '../../utils';
import CardContainer from './CardContainer';
import NothingToDisplay from './NothingToDisplay';

class Body extends PureComponent {
  static propTypes = {
    crew: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.shape({
        name: PropTypes.string,
        value: PropTypes.string,
      }),
      name: PropTypes.shape({
        first: PropTypes.string,
        last: PropTypes.string,
        title: PropTypes.string,
      }),
      picture: PropTypes.shape({
        large: PropTypes.string,
        medium: PropTypes.string,
        thumbnail: PropTypes.string,
      }),
    })),
  };

  state = {
    crewMembersByStage: null,
  };

  constructor(props) {
    super(props);

    this.move = this.move.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.crewMembersByStage === null &&
      Array.isArray(props.crew) &&
      props.crew.length > 0) {
      return {
        crewMembersByStage: props.crew.reduce((acc, cm) => {
          acc[STAGES.Applied][cm.id.value] = cm;

          return acc;
        }, { [STAGES.Applied]: {}, [STAGES.Interviewing]: {}, [STAGES.Hired]: {} }),
      };
    }

    return null;
  }

  move(id, prevStage, nextStage) {
    const crewMembersByStage = jsonCopy(this.state.crewMembersByStage);

    const member = crewMembersByStage[prevStage][id];
    delete crewMembersByStage[prevStage][id];
    crewMembersByStage[nextStage][id] = member;

    this.setState({
      crewMembersByStage,
    });
  }

  render() {
    const { crewMembersByStage } = this.state;
    if (crewMembersByStage) {
      const orderedStages = Object.keys(crewMembersByStage)
        .sort((a, b) => STAGE_ORDER.indexOf(a) - STAGE_ORDER.indexOf(b))
        .map(s => ({ label: s, data: Object.values(crewMembersByStage[s]) }));

      return (
        <div className="body">
          {orderedStages.map((s, idx) => (
            <span key={idx}>
              <h2>{s.label}</h2>
              <CardContainer stage={s.label} crew={s.data} move={this.move} />
            </span>
          ))}
        </div>
      );
    }

    return <NothingToDisplay />;
  }
}

export default Body;
