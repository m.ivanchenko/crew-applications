import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { STAGE_ORDER, MOVES } from '../../constants';
import Card from './Card';

class CardContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.move = this.move.bind(this);
  }

  static propTypes = {
    crew: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.shape({
        name: PropTypes.string,
        value: PropTypes.string,
      }),
      name: PropTypes.shape({
        first: PropTypes.string,
        last: PropTypes.string,
        title: PropTypes.string,
      }),
      picture: PropTypes.shape({
        large: PropTypes.string,
        medium: PropTypes.string,
        thumbnail: PropTypes.string,
      }),
    })),
    move: PropTypes.func,
    stage: PropTypes.string,
  };

  move(id, direction) {
    const { stage, move } = this.props;
    const currentIndex = STAGE_ORDER.indexOf(stage);

    let next;
    switch (direction) {
      case MOVES.Left: {
        if (currentIndex !== 0) {
          next = STAGE_ORDER[currentIndex - 1];
        }
        break;
      }

      case MOVES.Right: {
        if (currentIndex !== STAGE_ORDER.length - 1) {
          next = STAGE_ORDER[currentIndex + 1];
        }

        break;
      }
    }

    move(id, STAGE_ORDER[currentIndex], next);
  }

  render() {
    const { crew, stage } = this.props;

    const currentIndex = STAGE_ORDER.indexOf(stage);
    const moves = {
      [MOVES.Left]: currentIndex !== 0,
      [MOVES.Right]: currentIndex !== STAGE_ORDER.length - 1,
    };

    return (
      <div className="card-container">
        {crew.map((cm, idx) => <Card key={`${cm.id.value}-${idx}`} crewMember={cm} move={this.move} moves={moves} />)}
      </div>
    );
  }
}

export default CardContainer;
