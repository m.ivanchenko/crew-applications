import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { MOVES } from '../../constants';

class Card extends PureComponent {
  static propTypes = {
    crewMember: PropTypes.shape({
      id: PropTypes.shape({
        name: PropTypes.string,
        value: PropTypes.string,
      }),
      name: PropTypes.shape({
        first: PropTypes.string,
        last: PropTypes.string,
        title: PropTypes.string,
      }),
      picture: PropTypes.shape({
        large: PropTypes.string,
        medium: PropTypes.string,
        thumbnail: PropTypes.string,
      }),
    }),
    moves: PropTypes.arrayOf(PropTypes.number),
  };

  constructor(props) {
    super(props);

    this.move = this.move.bind(this);
  }

  move(direction) {
    const { value: id } = this.props.crewMember.id;

    this.props.move(id, direction);
  }

  render() {
    const { crewMember, moves } = this.props;

    return (
      <div className="card">
        <div className="name-first"> {crewMember.name.first}</div>
        <div className="name-last"> {crewMember.name.last}</div>
        <div className="buttons">
          {moves[MOVES.Left] && <div className="left" onClick={() => this.move(MOVES.Left)} />}
          {moves[MOVES.Right] && <div className="right" onClick={() => this.move(MOVES.Right)} />}
        </div>
      </div>
    );
  }
}

export default Card;
