import React from 'react';

export default function NothingToDisplay() {
  return (
    <div className="nothing-to-display">Nothing to display</div>
  );
}
