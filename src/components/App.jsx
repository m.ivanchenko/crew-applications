import React, { Component } from 'react';

import logo from '../assets/images/logo.png';
import '../assets/css/App.css';
import { DATA_ENDPOINT } from '../constants';
import Body from './body';

class App extends Component {
  state = {
    crew: [],
  };

  componentDidMount() {
    if (typeof (window.fetch) === 'function') {
      fetch(DATA_ENDPOINT)
        .then(response => {
          if (response.ok) {
            response.json()
              .then(data => this.setState({
                crew: data.results,
              }))
              .catch(e => console.error(e));
          }
        })
        .catch(ex => console.error(ex));
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">OpenOceanStudio: Crew Applications</h1>
        </header>
        <Body crew={this.state.crew} />
      </div>
    );
  }
}

export default App;
