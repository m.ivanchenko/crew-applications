export const DATA_ENDPOINT = 'https://randomuser.me/api/?nat=gb&results=5';

export const STAGES = {
  Applied: 'Applied',
  Hired: 'Hired',
  Interviewing: 'Interviewing',
};

export const STAGE_ORDER = [
  STAGES.Applied,
  STAGES.Interviewing,
  STAGES.Hired,
];

export const MOVES = {
  Left: 0,
  Right: 1,
};
